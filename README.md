# uniapp+vue3+ts+uviewPlus

#### 介绍
这是一个uniapp cli项目  主要框架 vue3+ts+ uview-plus+pinia

#### 软件架构
软件架构说明


#### 安装教程

1.  npm i
2.  npm run weixin(微信) ps:在manifest.json 添加自己的appid就可以运行到小程序
3.  npm run serve （h5）

### 公共组件说明
全局组件
<head-top :loading="loading"></head-top>  

### 接口请求 / 封装的接口请求方法
 ##  result.code 有一些人的状态吗成功不是 200的改这里
export function setApiData() {
  const { proxy }: any = getCurrentInstance()
  const store = componentsStore()
  const setApi = async (obj: any) => {

    let data: any = reactive({
      code: 500,
      msg: ''
    })
    try {
      if (!obj.unLoading) {
        store.loadingFn_(true)
        setNum.value++
      }
      const param = obj.param || []
      const result = await server[obj.name](...param)

      if (result.code === 200 || obj.isRoot) {   // 改这里
        let code: any = {}
        code = {
          code: 200
        }
        data = {
          ...code,
          ...result
        }
      } else {
        console.error(obj.title + " 请求出错", result.code, result.msg)
        data = {
          msg: result.msg,
          code: 500
        }

      }

    } catch (e) {
      console.error(obj.title + " 请求失败", e + "", e)

      proxy.showMsg.show({ text: obj.title })
    }
    if (!obj.unLoading) {
      setNum.value--;
      if (setNum.value === 0) {
        store.loadingFn_(false)
      }
    }
    // console.log(data, 'data')
    return data;

  }
  return { setApi }
}
import { setApiData } from "@/hooks";
 const  { setApi }  = setApiData()
 ## 结合 async /await 使用 
 const result = await setApi({
    title: '测试接口',
    name: 'supplier_shop_option',
    param: [{ id: 111111 }]
  })
  ## 传统方法
   //   setApi({  常规请求
  //   title: '测试接口',
  //   name: 'supplier_shop_option',
  //   param: [{ id: 111111 }]
  // }).then((e)=>{
  //   console.log(e,'e')
  // })
  console.log(result, 'res')
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
