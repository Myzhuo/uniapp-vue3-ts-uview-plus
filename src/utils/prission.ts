
import { ref } from 'vue'
import { get_, set_, clear_ } from '@/utils'
import { componentsStore } from '@/store/modules/components'
export default async function () {
  const store = componentsStore()
  const pageList = ['/pages/home/home']
  const list = ['navigateTo', 'redirectTo', 'reLaunch', 'switchTab']
  list.forEach(item => {
    uni.addInterceptor(item, {
      invoke(e) {
        store.isMsg_({})  // 进入新页面初始化loading
        console.log(e.url, item, '进入新页面')
        let url = ''
        // url = e.url.split('../..')[1].split('?')
        console.log(url)
        // if (!get_('Token') && url && !pageList.includes(url[0])) {
        //   store.isLoginFN(true)
        //   return false
        // } else {
        //   return true
        // }
        return true
      },
      fail(err) { // 失败回调拦截
        console.log(err, 'err')
      }
    })
  })
}