import { get_, set_, clear_ } from '@/utils'
import { componentsStore } from '@/store/modules/components'
import { store } from '../store'

let Times = 0  // 获取不到商家id时重新调用的次数
interface httpsObject {
  params: Array<[]>,
  method: string,
  url: string,
  data: object,
  header: object,
  timeout: number,
  code: number
}
const httpsFeatch = (url: string, opt: httpsObject) => {
  //包装请求对象
  let params = opt.params ? ('?' + Object.keys(opt.params).map(key => key + '=' + opt.params[key]).join('&')) : ''
  opt.method = opt.method || "GET";

  let header = {}
  if (get_('Token')) {
    let info = uni.getSystemInfoSync()
    header = { token: get_('Token'), deviceno: info.deviceId }
  }

  let api_data = { url_: url, data: opt.data, ...header, ...opt }
  // let url_https = ''
  let url_https = get_('APIHTTPS') && get_('APIHTTPS') || '/api'
  opt.url = url_https + url + params

  // #ifdef MP-WEIXIN
  if (!get_('Token')) {
    // if (!store.state.user.LodingModal) {
    //   store.commit('LoginModal', true)
    // }
    is_token(get_('Token'))
  }
  // #endif

  opt.header = { ...opt.header, ...header }
  opt.data = opt && opt.data || {};
  opt.timeout = 50000;
  //发起请求
  return new Promise((resolve, reject) => {
    const options: any = {}
    Object.keys(opt).map(key => {
      if (key !== "params") {
        return options[key] = opt[key]
      }
    })
    uni.request(options)
      .then(res => interceptorsRes(res, resolve, reject))
      .catch(err => interceptorsErr(err, api_data, reject))
  })
}
const interceptorsRes = (res: httpsObject, resolve: any, reject: any) => {
  const { code }: any = res.data
  const store = componentsStore()

  if (code === 401) {  //没有登录的时候直接回首页
    // #ifdef H5
    store.isMsg_({ text: '您好，你未登录..' })
    uni.navigateTo({
      url: 'pages/indexs/index'
    })
    // #endif
    clear_('Token')
    // #ifdef MP-WEIXIN || MP-TOUTIAO
    // store.isLoginFN(true)
    // #endif
    return
  }
  resolve(res.data)
}
//登录过期
const is_login = () => {
  uni.showModal({
    title: '提示',
    content: '登录过期',
    showCancel: false,
    success(res) {
      if (res.confirm) {
        clear_('Token')
        // store.dispatch('login_')
      }
    }
  })
}
// 检测没有登录
const is_token = (node: boolean) => {
  const store = componentsStore()
  let showMod = !node ? true : false
  let pages = getCurrentPages()
  const list = ['pages/index/index']
  let view = pages[pages.length - 1]
  if (!list.includes(view.route)) {
    store.isLoginFN(true)
  }

}
// 异常处理
const interceptorsErr = (err: string, api_data: object, reject: any) => {
  // store.dispatch('page_found')
  console.warn('请求出错', err)
  set_('api_data_url', api_data)
  const store = componentsStore()
  store.loadingFn_(false)
  reject(err)
}
export { httpsFeatch }
