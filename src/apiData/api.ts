import { post, get } from '@/apiData/request'
const api = {
  config_api: '/multi/'
}
interface apiData {
  data: object,
  config: any
}
const index_api = {  //index
  wxlogin(data: apiData, config: apiData) { //登录
    return post("mall/wx/login.json", data, config);
  },
  homeSubmit(data: apiData, config: apiData) { //home申请免单
    return post("mall/free/order/submit.json", data, config);
  },

}


export default {
  ...index_api,
}
