import { createSSRApp, ref, createVNode } from "vue";
import App from "./App.vue";
import uviewPlus from 'uview-plus'
import { setupStore } from './store/index'
import headTop from '@/components/loading/index.vue'
import BMoney from '@/components/BMoney/index.vue'
import { componentsStore } from '@/store/modules/components'


export function createApp() {
  const app = createSSRApp(App);
  app.use(uviewPlus)
  setupStore(app)
  app.component('BMoney', BMoney);
  app.component('headTop', headTop);
  app.config.globalProperties["showMsg"] = {
    show(opt) {
      const store = componentsStore()
      store.isMsg_(opt)
    }
  }
  return {
    app,
  };
}

