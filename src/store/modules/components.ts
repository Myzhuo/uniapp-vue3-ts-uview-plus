import { defineStore } from 'pinia';
import { set_, get_ } from '@/utils'

export const componentsStore = defineStore('componStore', {
  state: () => {
    return {
      headTop_: false,
      loading_: false,
      opts: {},
      cosUrl: 'https://taoaiimage.oss-cn-shanghai.aliyuncs.com/',
      isLogin: false,
      isAI_: false, //false 显示 俩个 true 显示单个
      toolbarIndex: 'index'
    }
  },
  actions: {
    isTabbarFn(index: string | number) {
      console.log(index, 'index_')
      this.toolbarIndex = index
    },
    loadingFn_(flag: boolean) {
      console.log(flag, '我进来了')
      this.loading_ = flag
    },
    headTopFn_(flag: boolean) {
      this.headTop = flag
    },
    isMsg_(node: any) {
      console.log(node, 'node')
      this.opts = node;
    },
    APIHTTPS() {
      const env = import.meta.env
      // console.log(env, 'VITE_ENVVITE_ENVVITE_ENV')
      if (env.MODE === "development") {
        // #ifdef MP-WEIXIN || MP-TOUTIAO
        set_('APIHTTPS', env.VITE_HTTPS_URL)
        // #endif
        // #ifdef H5
        // set_('Token', 'ea352acc-56d1-450a-ac1c-e09e0b630729')
        set_('APIHTTPS', '/api/')
        // #endif
      } else {
        set_('APIHTTPS', env.VITE_HTTPS_URL)
      }
    },
    isLoginFN(flag: boolean) {
      this.isLogin = flag
    },
    async uload_load() {
      try {
        // 获取版本更新信息
        const updateManager = uni.getUpdateManager();
        updateManager.onCheckForUpdate((res) => {
          console.log('小程序是否有版本更新 onLaunch', res.hasUpdate);
        });
        // 监听小程序有版本更新事件
        updateManager.onUpdateReady(() => {
          uni.showModal({
            title: '更新提示',
            content: '新版本已经准备好，现在就去体验吧！',
            showCancel: false,
            success(res) {
              if (res.confirm) {
                // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                updateManager.applyUpdate();
              }
            }
          });
        });
        // 监听小程序更新失败事件
        updateManager.onUpdateFailed(() => {
          // 新版本下载失败
          console.log('新版本下载失败');
        });
      } catch (err) {
        console.log('app onLaunch updateManager fail', err);
      }
    }

  }
}) 