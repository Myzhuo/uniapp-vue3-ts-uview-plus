export default {
  props: {
    list: {
      type: Array,
      default: []
    },
    direction: {
      type: String,
      default: 'row'
    },
    fontColor: {
      type: String,
    }
  }
}