
export default {
  props: {
    title: {  // 标题
      type: String,
      default: ''
    },
    width: {  //单位px
      type: [Number, String],
      default: 690
    },
    keyName: {  //key 左边的名字  value 内容的字段 is_copy 字段是否显示复制
      type: [Array, Object],
      default: () => [
        {
          key: '商家',
          value: 'supplier_name',
        },
        {
          key: '姓名',
          value: 'tourist_name',
        },
        {
          key: '订单编号',
          value: 'order_no',
          is_copy: true
        },
      ]
    },
    //  ps: 这里的值的只要有上面的key值就会自动匹配查找
    // obj = {
    //   tourist_name: '林卓',
    //   supplier_name: '全球e旅游',
    //   order_no: "PS20220802115522387b3d",
    //   add_time: 1662451066,
    //   area_type: 1,
    //   count_down: 1662452866,
    //   cs_telephone: "020-88811475",
    //   deduct: "",
    //   gift_data: [],
    //   gift_type: null,
    //   id: 8156,
    //   invoice_id: "",
    //   invoice_status: 1,
    //   invoice_status_key: 0,
    //   invoice_status_name: "待开票",
    //   is_affirm: 0,
    //   is_agent: 0,
    //   is_completion: 0,
    //   is_gift: 0,
    //   is_offer_invoice: 0,
    //   is_receive: 0,
    //   is_use: 0,
    //   mall_product_grounding: 1,
    //   mall_product_id: 259,
    //   multi_shop: null,
    //   order_mall_refund: null,
    //   order_mall_travel_info: [],
    // },
    keyValue: {   //      
      type: Object,
    }
  }
}