import { defineConfig, UserConfig, ConfigEnv, loadEnv } from 'vite'
import uni from "@dcloudio/vite-plugin-uni";
import path from 'path'
export default ({ mode }: ConfigEnv): UserConfig => {
  const env = loadEnv(mode, process.cwd())
  console.log(env, '________')
  return {
    plugins: [uni()],
    server: {
      open: true,
      port: 8997,
      proxy: { // H5跨域问题
        '/api': {
          target: env.VITE_HTTPS_URL,
          changeOrigin: true,
          rewrite: (path: string) => path.replace(/^\/api/, '')
        }
      }
    },
    resolve: {
      alias: {//添加别名
        '@': path.resolve(__dirname, 'src'),
        '@static': path.resolve(__dirname, 'src/static'),
      },
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `@import "@/style/index.scss";`,//引入全局样式
        },

      },
    },

    // build: {
    //   minify: 'terser',
    //   terserOptions: {
    //     compress: {
    //       drop_console: true,
    //     },
    //   },
    // },
  }
}